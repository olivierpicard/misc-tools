display_help()
{
    echo 'Get tags for a given image from registry.hub.docker.com/v2/
ARGS:
    - Name of the image you want to get a tag list
    - Number of tags you want to see
    '
}
case $1 in 
    -h|--help) display_help
    exit 0
    ;;
esac


number=$2
if [ -z "$number" ]
then
    number=30
fi 

curl https://registry.hub.docker.com/v2/repositories/library/$1/tags/?page_size=$number 2>/dev/null |`` jq '."results"[]["name"]'